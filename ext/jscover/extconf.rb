require 'mkmf'
require 'mini_portile'

recipe = MiniPortile.new "jscover-native", "1.0.0"
recipe.files << 'https://bitbucket.org/jameshu/jscover-native/get/master.tar.gz'

checkpoint = "ports/.#{recipe.name}-#{recipe.version}-#{recipe.host}.installed"
unless File.exist?(checkpoint)
  recipe.configure_options = 'CFLAGS=-fPIC CPPFLAGS=-fPIC'
  recipe.cook
  File.write(checkpoint, '')
end

recipe.activate

$LIBPATH << File.join(recipe.path, 'lib')
$INCFLAGS = $INCFLAGS + " -I#{File.join(recipe.path, 'include')}"
$LIBS = $LIBS + " -ljscoverage -ljs_static -lm -lpthread -lstdc++"

create_makefile('jscover/jscover')
